<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PapelController;
use App\Http\Controllers\Api\ModeloController;
use App\Http\Controllers\Api\CorreioController;
use App\Http\Controllers\Api\EmpresaController;
use App\Http\Controllers\Api\EtiquetaControler;
use App\Http\Controllers\Api\UsuarioController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\SolicitacaoController;
use App\Http\Controllers\Api\UsuarioTempController;
use App\Http\Controllers\Api\Auth\RegistroController;
use App\Http\Controllers\Api\CampoVariavelController;
use App\Http\Controllers\Api\UtilitarioController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'prefix' => 'v1',
        'middleware' => [
            'auth:sanctum'
        ]
    ],
    function () {
        Route::resource('usuarios', 'Api\UsuarioController');
        Route::resource('clientes', 'Api\ClienteController');
        Route::resource('empresas', 'Api\EmpresaController');
        Route::resource('posts', 'Api\PostController');
        Route::resource('papeis', 'Api\PapelController');
        Route::resource('enderecos', 'Api\EnderecoController');
        Route::resource('centroCustos', 'Api\CentroCustoController');
        Route::resource('produtos', 'Api\ProdutoController');
        Route::resource('tiposProdutos', 'Api\ProdutoController');
        Route::resource('etiquetas', 'Api\EtiquetaController');
        Route::resource('tipoEntregas', 'Api\TipoEntregaController');
        Route::resource('modelos', 'Api\ModeloController');
        Route::resource('camposVariaveis', 'Api\CampoVariavelController');
        Route::resource('vendas', 'Api\VendalController');
        Route::resource('remessas', 'Api\RemessaController');
        Route::resource('solicitacoes', 'Api\SolicitacaoController');

        Route::get('/revoga-token', [UsuarioController::class, 'revogaToken']);
        Route::get('/carrega-usuario', [UsuarioController::class, 'carregaUsuario']);
        Route::get('/selecionar-empresa', [UsuarioController::class, 'selecionarEmpresa']);
        Route::get('/papel-permissoes', [PapelController::class, 'papelPermissoes']);
        Route::get('/permissoes-usuario', [UsuarioController::class, 'permissoesUsuario']);
        Route::get('/papeis-usuario/{identify}', [PapelController::class, 'papeisUsuario']); //mexer depois tem que carregar pelo idennty
        Route::get('/empresas-usuarios/{identify}', [EmpresaController::class, 'empresasUsuario']);
        Route::get('/empresas-cliente/{identify}', [EmpresaController::class, 'empresasCliente']);
        Route::post('/importar-etiquetas', [EtiquetaControler::class, 'importarEtiquetas']);
        Route::get('/rastrear-encomenda', [CorreioController::class, 'rastrearEncomendaCorreios']);
        Route::get('/buscar-cep', [CorreioController::class, 'buscarCep']);
        Route::get('/campos-variaveis-modelo', [ModeloController::class, 'camposVariaveisModelo']);
        Route::get('/exportar-modelo', [ModeloController::class, 'exportarModelo']);
        Route::get('/selecionar-modelo', [ModeloController::class, 'selecionarModelo']);
        Route::get('/exportar-solicitacoes', [SolicitacaoController::class, 'exportarSolicitacoes']);
        Route::post('/importar-solicitacoes', [SolicitacaoController::class, 'importarSolicitacoes']);
        Route::get('/vincular-empresa', [UsuarioController::class, 'vincularEmpresa']);
        Route::get('/lista-campos-autenticacao', [EmpresaController::class, 'listaCamposAutenticacao']);
        Route::post('/criar-campo-autenticacao', [EmpresaController::class, 'criarCampoAutenticacao']);
        Route::get('/excluir-campo-autenticacao/{id}', [EmpresaController::class, 'excluirCampoAutenticacao']);
        Route::get('/remover-fundo-foto-usuario', [UsuarioTempController::class, 'removerFundo']);
        Route::post('/upload-imagem-usuario', [UsuarioTempController::class, 'uploadImagemUsuario']);
        Route::post('/upload-imagem-crop', [UsuarioTempController::class, 'uploadImagemCrop']);
        Route::get('/carrega-usuario-temp', [UsuarioTempController::class, 'carregaUsuario']);
        Route::get('/lista-nomes', [UsuarioTempController::class, 'listaNomes']);
        Route::post('/confirmar-nome', [UsuarioTempController::class, 'confirmarNome']);
        Route::get('/mudar-etapa', [UsuarioTempController::class, 'mudarEtapa']);
        Route::get('/carrega-imagem-original', [UsuarioTempController::class, 'carregaImagemOriginal']);        
        Route::get('/remover-fundo-imagem', [UsuarioTempController::class, 'removerFundoImagem']);        
        Route::get('/refazer-solicitacao', [UsuarioTempController::class, 'refazerSolicitacao']);
        Route::get('/configuracao-modelo', [UsuarioTempController::class, 'configuracaoModelo']);
        Route::get('/finalizar-envio', [UsuarioTempController::class, 'finalizarEnvio']);
        Route::post('/utils/inportar-planilha-tmt', [UtilitarioController::class, 'importarPlanilhaRemessa']);
    }
);

/*Route::middleware('auth:sanctum')->group(function () {
    Route::resource('usuarios', 'Api\UsuarioController');
    Route::resource('empresas', 'Api\EmpresaController');
    Route::get('/revoga-token', [UsuarioController::class, 'revogaToken']);
});*/

/**
 * Auth and Register Routes
 */
Route::post('/registro', [RegistroController::class, 'store']);

Route::post('/login', [AuthController::class, 'login']);
Route::post('/autenticar', [UsuarioTempController::class, 'autenticar']);
Route::get('/logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
Route::get('/usuario-autenticado', [AuthController::class, 'me'])->middleware('auth:sanctum');
Route::get('/v1/info-empresa', [EmpresaController::class, 'infoEmpresa']);
