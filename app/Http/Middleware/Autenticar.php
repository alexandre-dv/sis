<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Permissao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class Autenticar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){

        if (auth()->check()) {
            foreach ($this->listaPermissoes() as $permissao) {
                Gate::define($permissao->nome, function ($usuario) use ($permissao) {
                    return $usuario->temUmPapelDestes($permissao->papeis) || $usuario->isAdmin();
                });
            }
        }
        return $next($request);
    }

    public function listaPermissoes()
    {
        return Permissao::with('papeis')->get();
    }
}
