<?php

namespace App\Http\Middleware\Empresa;

use Closure;
use Illuminate\Http\Request;
use App\Empresa\GerenciarEmpresa;
use Illuminate\Support\Facades\Auth;

class EmpresaFileSystems
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){      
        //dd(Auth::guard(session()->get('guard'))->check());
        if (Auth::guard(session()->get('guard'))->check())
            $this->setFilesystemsRoot();

        return $next($request);
    }

    public function setFilesystemsRoot()
    {
        $empresa = app(GerenciarEmpresa::class)->getEmpresa();

        config()->set(
            'filesystems.disks.empresa.root',
            config('filesystems.disks.empresa.root') . "/{$empresa->uuid}"
        );

    }
}
