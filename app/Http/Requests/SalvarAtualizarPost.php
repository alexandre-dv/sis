<?php

namespace App\Http\Requests;

use App\Rules\Empresa\EmpresaUnique;
use Illuminate\Foundation\Http\FormRequest;

class SalvarAtualizarPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => [
                'required',
                'min:3',
                'max:100',
                //new EmpresaUnique('posts', $this->segment(2)),
            ],
            'descricao' => 'required|max:10000',
        ];
    }
}
