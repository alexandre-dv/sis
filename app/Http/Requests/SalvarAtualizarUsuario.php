<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalvarAtualizarUsuario extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $uuid = $this->usuario;

        $rules = [
            'nome' => ['required', 'string', 'min:3', 'max:100'],
            'cpf' => ['required'],
            'empresa_id' => ['required'],
            'password' => ['required', 'min:4', 'max:16'],
            'email' => ['required', 'email', 'max:255', "unique:usuarios,email,{$uuid},uuid"],
        ];

        if($this->method() == 'PUT'){
            $rules['password'] = ['nullable', 'min:4', 'max:16'];
            $rules['email'] = ['required', 'email', 'max:255'];
        }

        return $rules;
    }

    public function messages() {
        return [
            'required' => 'Este campo é obrigatório',
            'email' => 'O campo :attribute deve conter um endereço de e-mail válido.',
            "email.unique" => "Este endereço de e-mail já esta em uso" ,
            'max' => 'O :attribute não pode ser maior que :max.',
            'min' => 'O :attribute deve conter no mínimo :min caracteres',
            'confirmed' => 'A confirmação da senha não corresponde.',
            'accepted' => 'Você deve concordar com os termos e condições antes de enviar.'
        ];
    }
}
