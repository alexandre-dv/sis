<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class AuthUsuario extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email', 'max:100','min:3'],
            'password' => ['required', 'min:4', 'max:16'],
            //'device_name' => ['required', 'string', 'max:200']
        ];
    }

    public function messages() {
        return [
            'required' => 'Este campo é obrigatório',
            'email' => 'O campo :attribute deve conter um endereço de e-mail válido.',
            "email.unique" => "Este endereço de e-mail já esta em uso" ,
            'max' => 'O :attribute não pode ser maior que :max.',
            'min' => 'O :attribute deve conter no mínimo :min caracteres',
            'confirmed' => 'A confirmação da senha não corresponde.',
            'accepted' => 'Você deve concordar com os termos e condições antes de enviar.'
        ];
    }
}
