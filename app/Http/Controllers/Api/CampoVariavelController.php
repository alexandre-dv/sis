<?php

namespace App\Http\Controllers\Api;

use App\Exports\ModeloExport;
use Illuminate\Http\Request;
use App\Models\CampoVariavel;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Resources\CampoVariavelResource;

class CampoVariavelController extends Controller
{

    protected $campoVariavel;

    public function __construct(CampoVariavel $campoVariavel)
    {
        $this->campoVariavel = $campoVariavel;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campoVariavel = $this->campoVariavel->paginate();
        return CampoVariavelResource::collection($campoVariavel);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campoVariavel = $this->campoVariavel->create([
            'nome_label' => $request->camel_case == 1 ? ucwords($request->nome_label) : $request->nome_label,
            'nome_tecnico' => str_replace(" ", "_", $request->nome_tecnico),
            'modelo_id' => $request->modelo_id,
            'modelo_id' => $request->modelo_id,
            'campo_chave' => $request->campo_chave,
            'presente_planilha_modelo' => $request->presente_planilha_modelo,
            'presente_criar_solicitacao' => $request->presente_criar_solicitacao
        ]);
        return new CampoVariavelResource($campoVariavel);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campoVariavel = $this->campoVariavel->find($id);
        return new CampoVariavelResource($campoVariavel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campoVariavel = $this->campoVariavel->find($id);
        $campoVariavel->update([
            'nome_label' => $request->camel_case == 1 ? ucwords($request->nome_label) : $request->nome_label,
            'nome_tecnico' => str_replace(" ", "_", $request->nome_tecnico),
            'modelo_id' => $request->modelo_id,
            'modelo_id' => $request->modelo_id,
            'campo_chave' => $request->campo_chave,
            'presente_planilha_modelo' => $request->presente_planilha_modelo,
            'presente_criar_solicitacao' => $request->presente_criar_solicitacao
        ]);
        
        return new CampoVariavelResource($campoVariavel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campoVariavel = $this->campoVariavel->find($id);
        $campoVariavel->delete();

        return response()->json([
            'mensagem' => "Registro excluido com sucesso"
        ], 200);
    }
}
