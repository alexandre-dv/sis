<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\VendaResource;
use App\Models\Venda;
use Illuminate\Http\Request;

class VendaController extends Controller
{

    protected $venda;

    public function __construct(Venda $venda)
    {
        $this->venda = $venda;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendas = $this->venda->paginate();
        return VendaResource::collection($vendas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $venda = $this->venda->create($request->all());
        return new VendaResource($venda);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venda = $this->venda->find($id);
        return new VendaResource($venda);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $venda = $this->venda->find($id);
        $venda->update($request->all());
        return new VendaResource($venda);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venda = $this->venda->find($id);
        $venda->delete();

        return response()->json([
            'mensagem' => "Venda excluida com sucesso"
        ], 200);
    }
}
