<?php

namespace App\Http\Controllers\Api;

use App\Models\CentroCusto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CentroCustoResource;

class CentroCustoController extends Controller
{

    protected $centroCusto;

    public function __construct(CentroCusto $centroCusto)
    {
        $this->centroCusto = $centroCusto;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $centroCustos = $this->centroCusto->paginate();
        return CentroCustoResource::collection($centroCustos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $centroCusto = $this->centroCusto->create($request->all());
        return new CentroCustoResource($centroCusto);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $centroCusto = $this->centroCusto->find($id);
        return new CentroCustoResource($centroCusto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $centroCusto = $this->centroCusto->find($id);
        $centroCusto->update($request->all());
        return new CentroCustoResource($centroCusto);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $centroCusto = $this->centroCusto->find($id);
        $centroCusto->delete();

        return response()->json([
            'mensagem' => "Centro de custos excluido com sucesso"
        ], 200);
    }
}
