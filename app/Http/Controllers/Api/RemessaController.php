<?php

namespace App\Http\Controllers\Api;

use App\Models\Remessa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\RemessaResource;

class RemessaController extends Controller{
    
    protected $remessa;

    public function __construct(Remessa $remessa)
    {
        $this->remessa = $remessa;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $remessas = $this->remessa->paginate();
        return RemessaResource::collection($remessas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add([
            'usuario_id' => Auth::user()->id,
            'status' => $request->em_criacao==1 ? 0 : 1,
            'modelo_id' =>  session()->get('modelo.modelo_id')
        ]);

        $remessa = $this->remessa->create($request->all());        
        return new RemessaResource($remessa);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $remessa = $this->remessa->find($id);
        return new RemessaResource($remessa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $remessa = $this->remessa->find($id);
        $remessa->update($request->all());
        return new RemessaResource($remessa);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $remessa = $this->remessa->find($id);
        $remessa->delete();

        return response()->json([
            'mensagem' => "Remessa excluida com sucesso"
        ], 200);
    }
}
