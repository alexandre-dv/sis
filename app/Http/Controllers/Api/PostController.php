<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\SalvarAtualizarPost;

class PostController extends Controller
{
    protected $post;

    function __construct(Post $post)
    {
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        if(Gate::denies('listar-posts')){
            abort( response()->json([
                "erro" =>'Acesso não autorizado'
            ], 401) );
        }

        $posts = $this->post->paginate();
        return PostResource::collection($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalvarAtualizarPost $request){

        if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {
            $nome = Str::kebab($request->titulo);
            $extensao = $request->imagem->extension();
            $nomeImagem = "{$nome}.$extensao";
            $data['imagem'] = $nomeImagem;
            $upload = $request->imagem->storeAs('posts', $nomeImagem);

            if (!$upload)
                return response()->json([
                    "erro" => "falaha no upload"
                ]);
        }

        $post = $this->post->create($request->all());
        return new PostResource($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->post->find($id);
        return new PostResource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SalvarAtualizarPost $request, $id)
    {
        $post = $this->post->find($id);
        $post->update($request->all());
        return new PostResource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->post->find($id);
        $post->delete();
        return response()->json([
            'mensagem' => "Post excluido com sucesso"
        ], 200);
    }
}
