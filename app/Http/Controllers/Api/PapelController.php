<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PapelResource;
use App\Http\Resources\PermissaoResource;
use App\Models\Papel;
use App\Models\Usuario;
use Illuminate\Http\Request;

class PapelController extends Controller
{

    protected $papel, $usuario;

    public function __construct(Papel $papel, Usuario $usuario)
    {
        $this->papel = $papel;
        $this->usuario = $usuario;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $papeis = $this->papel->paginate();
        return PapelResource::collection($papeis);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $papel = $this->papel->create($request->all());
        return new PapelResource($papel);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $papel = $this->papel->find($id);
        return new PapelResource($papel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $papel = $this->papel->find($id);
        if ($papel) {
            $papel->update($request->all());
            return new PapelResource($papel);
        } else {
            return response()->json([
                'erro' => "Registro não encontrado"
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $papel = $this->papel->find($id);
        $papel->delete();
        return response()->json([
            'sucesso' => "Papel excluido com sucesso"
        ], 200);
    }

    public function papelPermissoes(Request $request)
    {
        $papel = Papel::with('permissoes')
            ->where('id', $request->papel_id)
            ->first();

        return PermissaoResource::collection($papel->permissoes);
    }

    public function papeisUsuario($identify)
    {
        $usuario = $this->usuario->where('uuid', $identify)->first();

        $where = function ($q) use ($usuario) {
            $q->whereHas('usuariosPapeis', function ($q) use ($usuario) {
                $q->where('usuario_id', $usuario->id);
            });
        };

        $papeis = $this->usuario->where($where)->get();
        return PapelResource::collection($papeis);
    }
}