<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Papel;
use App\Models\Empresa;
use App\Models\Usuario;
use App\Models\Permissao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\EmpresaResource;
use App\Http\Resources\UsuarioResource;
use App\Http\Resources\PermissaoResource;
use App\Http\Requests\SalvarAtualizarUsuario;

class UsuarioController extends Controller
{
    protected $usuario, $empresa, $papel, $permissao;

    public function __construct(Usuario $usuario, Empresa $empresa, Papel $papel, Permissao $permissao)
    {
        $this->usuario = $usuario;
        $this->empresa = $empresa;
        $this->papel = $papel;
        $this->permissao = $permissao;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usuarios = $this->usuario->paginate($request->registros_por_pagina);

        $usuarios->getCollection()->transform(function ($usuario){ 
            $data = [
                'id' => $usuario->uuid,
                'nome' => $usuario->nome,
                'email' => $usuario->email,
                'foto_perfil' => $usuario->foto_usuario,
                'data' => Carbon::createFromFormat("Y-m-d H:i:s", $usuario->created_at, 'America/Sao_Paulo')->format('d/m/Y H:i')
            ];
            return $data;
        });

        return UsuarioResource::collection($usuarios);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalvarAtualizarUsuario $request){        
        config()->set('filesystems.disks.empresa.root', config('filesystems.disks.public.root'));
        $data = $request->validated();
                
        //Gerar senha
        $data['password'] = bcrypt($data['password']);      
        $data['device_name'] = $request->header('User-Agent');        

        //salvar foto do perfil
        if ($request->hasFile('foto_perfil') && $request->file('foto_perfil')->isValid()) {
            $nome = sha1(microtime());
            $extensao = $request->foto_perfil->extension();
            $nomeImagem = "{$nome}.$extensao";            
            $upload = $request->foto_perfil->storeAs("usuarios/", $nomeImagem);

            if (!$upload){
                return response()->json([
                    "erro" => "falaha no upload"
                ], 500);
            } else {                                
                $data['foto_perfil'] = $nomeImagem;  
            }
        }

        $usuario = $this->usuario->create($data);
        
        //vincular empresa
        $empresa = $this->empresa->where('uuid', $request->empresa_id)->first();        
        $usuario->empresasUsuarios()->attach($empresa->id);
        $usuario->update([
            'empresa_selecionada' => $empresa->id
        ]);

        return new UsuarioResource($usuario);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $identify
     * @return \Illuminate\Http\Response
     */
    public function show($uuid){   
        $usuario = $this->usuario->where('uuid', $uuid)->firstOrFail();
        return new UsuarioResource($usuario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $identify
     * @return \Illuminate\Http\Response
     */
    public function update(SalvarAtualizarUsuario $request, $identify)
    {
        $usuario = $this->usuario->where('uuid', $identify)->firstOrFail();

        $data = $request->validated();

        if ($request->password) {
            $data['password'] = bcrypt($request->password);
        }

        $usuario->update($data);

        return response()->json([
            'atualizado' => 'sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $identify
     * @return \Illuminate\Http\Response
     */
    public function destroy($identify)
    {
        $usuario = $this->usuario->where('uuid', $identify)->firstOrFail();
        $usuario->delete();

        return response()->json([
            'deletado' => 'sucesso'
        ]);
    }

    public function selecionarEmpresa(Request $request){

        $empresa = $this->empresa->where('uuid', $request->empresa_id)->first();
        
        if ($empresa) {
            session([
                'empresa' => [
                    'empresa_id' => $empresa->id,
                    'uuid' => $empresa->uuid
                ]
            ]);

            $usuario = $this->usuario->find(Auth()->user()->id);
            $usuario->update([
                'empresa_selecionada'=> $empresa->id
            ]);

            return new EmpresaResource($empresa);
        } else {
            return response()->json([
                'error' => "Empresa não encotrada"
            ], 404);
        }
    }

    public function revogaToken(Request $request)
    {

        $usuario = $this->usuario->where('id', 15)->first();

        //pegar token do usuario
        //$usuario->tokens()->first()->token;

        // Revogar todos os tokens.
        //$usuario->tokens()->delete();

        // Revogar o token que foi usado para autenticar a solicitação atual.
        //$request->user()->currentAccessToken()->delete();

        // Revogar um token específico.
        $tokenId = 'c50a3cdbb086d279ccc329016d4ecca56a85dc0a4b3c091c86ed75e2938cf59e';
        $usuario->tokens()->where('token', $tokenId)->delete();
    }

    public function permissoesUsuario(Request $request)
    {
        $whereUsuario = function ($q) {
            $q->whereHas('papeisUsuarios', function ($q) {
                $q->where('usuario_id', Auth::user()->id);
            });
        };

        $papeis = $this->papel->where($whereUsuario)->pluck('id');

        $wherePapeis = function ($q) use ($papeis) {
            $q->whereHas('permissoesPapeis', function ($q) use ($papeis) {
                $q->whereIn('papel_id', $papeis);
            });
        };

        $permissoes = $this->permissao->where($wherePapeis)->pluck('nome');

        return new PermissaoResource($permissoes);
    }

    public function vincularEmpresa(Request $request){

        $empresa = $this->empresa->where('uuid', $request->empresa_id)->first();   
        $usuario = $this->usuario->where('uuid', $request->usuario_id)->first();  

        $where = function($q) use($usuario, $empresa){
            $q->whereHas('empresasUsuarios', function($q) use($usuario, $empresa){
                $q->where('usuario_id', $usuario->id)
                ->where('empresa_id', $empresa->id);
            });
        };
        $usuarioEmpresa = Usuario::where($where)->first();
        if($usuarioEmpresa == null){
            $usuario->empresasUsuarios()->attach($empresa->id);
            $data = [
                'sucesso' => "O usuario foi vinculado a empresa com sucesso!"
            ];            
            return new UsuarioResource($data);
        }else{
            return response()->json([
                'erro' => "Este usuario já pertence a esta empresa"
            ], 500);
        }
    }

    public function carregaUsuario(){       
        $usuario = Usuario::find(Auth::guard()->user()->id);        
        return new UsuarioResource($usuario);
    }
}