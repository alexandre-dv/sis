<?php

namespace App\Http\Controllers\Api;

use App\Models\Etiqueta;
use Illuminate\Http\Request;
use App\Imports\EtiquetasImport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Resources\EtiquetaResource;

class EtiquetaControler extends Controller
{
    protected $etiqueta;

    public function __construct(Etiqueta $etiqueta)
    {
        $this->etiqueta = $etiqueta;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $etiquetas = $this->etiqueta->paginate();
        return EtiquetaResource::collection($etiquetas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $etiqueta = $this->etiqueta->create($request->all());
        return new EtiquetaResource($etiqueta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $etiqueta = $this->etiqueta->find($id);
        return new EtiquetaResource($etiqueta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $etiqueta = $this->etiqueta->find($id);
        $etiqueta->update($request->all());
        return new EtiquetaResource($etiqueta);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $etiqueta = $this->etiqueta->find($id);
        $etiqueta->delete();

        return response()->json([
            'mensagem' => "Registro excluido com sucesso"
        ], 200);
    }

    public function importarEtiquetas(){
        $import = new EtiquetasImport();
        Excel::import($import, request()->file('file'));
        dd($import->etiquetas());
    }
}
