<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\SalvarUsuario;
use App\Http\Resources\UsuarioResource;

class RegistroController extends Controller
{
    protected $model;

    public function __construct(Usuario $usuario)
    {
        $this->model = $usuario;
    }


    public function store(SalvarUsuario $request)
    {
        $data = $request->validated();
        $data['password'] = bcrypt($data['password']);
        $user = $this->model->create($data);

        return (new UsuarioResource($user))
            ->additional([
                'token' => $user->createToken($request->device_name)->plainTextToken,
            ]);
    }
}
