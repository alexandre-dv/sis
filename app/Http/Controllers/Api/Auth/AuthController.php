<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Auth\AuthUsuario;
use App\Http\Resources\UsuarioResource;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    protected $usuario;

    public function __construct(Usuario $usuario)
    {
        $this->usuario = $usuario;
    }

    public function login(AuthUsuario $request){

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            $request->request->add([
                'device_name' => $request->header('User-Agent')
            ]);

            $usuario = $this->usuario->where('email', $request->email)->first();

            if (!$usuario || !Hash::check($request->password, $usuario->password)) {
                throw ValidationException::withMessages([
                    'email' => ['As credenciais fornecidas estão incorretas.'],
                ]);
            }

            // Revogar todos os tokens.
            $usuario->tokens()->delete();

            activity('Login')->by($usuario)
                ->withProperties([
                    'email' => $usuario->email,
                    'device_name' => $request->header('User-Agent')
                ])
                ->log('login realizado com sucesso');

            return (new UsuarioResource($usuario))->additional([
                    'token' => $usuario->createToken($request->header('User-Agent'))->plainTextToken        
                ]);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        return response()->json([
            'logout' => 'success'
        ]);
    }

    public function me(Request $request){
        $user = $request->user();
        return new UsuarioResource($user);
    }
}
