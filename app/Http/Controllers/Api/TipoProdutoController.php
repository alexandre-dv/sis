<?php

namespace App\Http\Controllers\Api;

use App\Models\TipoProduto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TipoProdutoResource;

class TipoProdutoController extends Controller
{

    protected $tipoProduto;

    public function __construct(TipoProduto $tipoProduto)
    {
        $this->TipoProduto = $tipoProduto;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiposProduto = $this->tipoProduto->paginate();
        return TipoProdutoResource::collection($tiposProduto);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipoProduto = $this->tipoProduto->create($request->all());
        return new TipoProdutoResource($tipoProduto);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipoProduto = $this->tipoProduto->find($id);
        return new TipoProdutoResource($tipoProduto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipoProduto = $this->tipoProduto->find($id);
        $tipoProduto->update($request->all());
        return new TipoProdutoResource($tipoProduto);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipoProduto = $this->tipoProduto->find($id);
        $tipoProduto->delete();

        return response()->json([
            'mensagem' => "Tipo de Produto excluido com sucesso"
        ], 200);
    }
}
