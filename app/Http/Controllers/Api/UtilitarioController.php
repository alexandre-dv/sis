<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Utilitario;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\utils\PlanilhaRemessaImport;

class UtilitarioController extends Controller{

    protected $utilitario;

    public function __construct(Utilitario $utilitario){
        $this->utilitario = $utilitario;        
    }

    public function importarPlanilhaRemessa(Request $request){
        return response()->json(['results' => 'teste'],200);
        try {                 
            #$import = new PlanilhaRemessaImport();
            #Excel::import($import, request()->file('arquivo'));
            $data = Excel::toArray(new PlanilhaRemessaImport(), request()->file('arquivo'));  
            return $data;         
            #return response()->json(['results' => $data],200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()],500);
        }
    }
}
