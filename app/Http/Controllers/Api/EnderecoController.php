<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EnderecoResource;
use App\Models\Endereco;
use Illuminate\Http\Request;

class EnderecoController extends Controller
{

    protected $endereco;

    public function __construct(Endereco $endereco)
    {
        $this->endereco = $endereco;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enderecos = $this->endereco->paginate();
        return EnderecoResource::collection($enderecos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $endereco = $this->endereco->create($request->all());
        return new EnderecoResource($endereco);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $endereco = $this->endereco->find($id);
        return new EnderecoResource($endereco);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $endereco = $this->endereco->find($id);
        $endereco->update($request->all());
        return new EnderecoResource($endereco);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $endereco = $this->endereco->find($id);
        $endereco->delete();

        return response()->json([
            'mensagem' => "Registro excluido com sucesso"
        ], 200);
    }
}