<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TipoEntregaResource;
use App\Models\TipoEntrega;
use Illuminate\Http\Request;

class TipoEntregaController extends Controller
{
    protected $tipoEntrega;

    public function __construct(TipoEntrega $tipoEntrega)
    {
        $this->tipoEntrega = $tipoEntrega;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipoEntregas = $this->tipoEntrega->paginate();
        return TipoEntregaResource::collection($tipoEntregas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipoEntrega = $this->tipoEntrega->create($request->all());
        return new TipoEntregaResource($tipoEntrega);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipoEntrega = $this->tipoEntrega->find($id);
        return new TipoEntregaResource($tipoEntrega);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipoEntrega = $this->tipoEntrega->find($id);
        $tipoEntrega->update($request->all());
        return new TipoEntregaResource($tipoEntrega);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipoEntrega = $this->tipoEntrega->find($id);
        $tipoEntrega->delete();

        return response()->json([
            'mensagem' => "Registro excluido com sucesso"
        ], 200);
    }
}
