<?php

namespace App\Http\Controllers\Api;

use App\Models\Cliente;
use App\Models\Empresa;
use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmpresaResource;
use App\Models\CampoAutenticacao;
use App\Models\EmpresaAutenticacao;
use App\Models\Remessa;
use Illuminate\Support\Facades\Auth;

class EmpresaController extends Controller
{

    protected $empresa, $usuario, $cliente;

    public function __construct(Empresa $empresa, Usuario $usuario, Cliente $cliente)
    {
        $this->empresa = $empresa;
        $this->usuario = $usuario;
        $this->cliente = $cliente;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = $this->empresa->paginate();

        return EmpresaResource::collection($empresas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $empresa = $this->empresa->create($request->all());
        $data =[
            'identify' => $empresa->uuid,
            'nome_fantasia' => $empresa->nome_fantasia,
            'razao_social' => $empresa->razao_social,
            'inscricao_estadual' => $empresa->inscricao_estadual,
            'cnpj' => $empresa->cnpj,
            'endereco' =>$empresa->endereco
        ];
        return new EmpresaResource($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($identify)
    {
        $empresa = $this->empresa->where('uuid', $identify)->first();
        $data =[
            'identify' => $empresa->uuid,
            'nome_fantasia' => $empresa->nome_fantasia,
            'razao_social' => $empresa->razao_social,
            'inscricao_estadual' => $empresa->inscricao_estadual,
            'cnpj' => $empresa->cnpj,
            'endereco' =>$empresa->endereco
        ];
        return new EmpresaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $identify)
    {
        $empresa = $this->empresa->where('uuid', $identify)->firstOrFail();
        $empresa->update($request->all());
        $data =[
            'identify' => $empresa->uuid,
            'nome_fantasia' => $empresa->nome_fantasia,
            'razao_social' => $empresa->razao_social,
            'inscricao_estadual' => $empresa->inscricao_estadual,
            'cnpj' => $empresa->cnpj,
            'endereco' =>$empresa->endereco
        ];
        return new EmpresaResource($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = $this->empresa->find($id);
        $empresa->delete();

        return response()->json([
            'mensagem' => "Empresa excluida com sucesso"
        ], 200);
    }

    public function empresasUsuario($identify)
    {
        $usuario = $this->usuario->where('uuid', $identify)->first();

        $where = function ($q) use ($usuario) {
            $q->whereHas('usuariosEmpresas', function ($q) use ($usuario) {
                $q->where('usuario_id', $usuario->id);
            });
        };

        $empresas = $this->empresa->with('enderecos')->where($where)->get();
        foreach ($empresas as $empresa) {
            $data[] = [
                'identify' => $empresa->uuid,
                'nome_fantasia' => $empresa->nome_fantasia,
                'razao_social' => $empresa->razao_social,
                'inscricao_estadual' => $empresa->inscricao_estadual,
                'cnpj' => $empresa->cnpj,
                'endereco' => $empresa->endereco
            ];
        }
        return EmpresaResource::collection($data);
    }

    public function empresasCliente($identify)
    {
        //lista todas as empresas do cliente
        $cliente = $this->cliente->where('uuid', $identify)->first();
        $empresas = $this->empresa->where('cliente_id', $cliente->id)->get();
        foreach ($empresas as $empresa) {
            $data[] = [
                'identify' => $empresa->uuid,
                'nome_fantasia' => $empresa->nome_fantasia,
                'razao_social' => $empresa->razao_social,
                'inscricao_estadual' => $empresa->inscricao_estadual,
                'cnpj' => $empresa->cnpj,
                'endereco' => $empresa->endereco
            ];
        }
        return EmpresaResource::collection($data);
    }

    public function listaCamposAutenticacao(){
        $camposAutenticacao = CampoAutenticacao::get();
        foreach($camposAutenticacao as $campo){
            $data[] = [
                'nome' => $campo->nome
            ];
        }
        return new EmpresaResource($data);
    }

    public function criarCampoAutenticacao(Request $request){
       $campoAutenticacao = CampoAutenticacao::create($request->all());
       $data = [
           'nome' => $campoAutenticacao->nome
       ];       
        return new EmpresaResource($data);
    }

    public function excluirCampoAutenticacao($id){
        $camposAutenticacao = CampoAutenticacao::find($id);
        $camposAutenticacao->delete();

        return response()->json([
            'mensagem' => "Registro excluido com sucesso"
        ], 200);

    }

    public function infoEmpresa(){
        
        $empresa = Empresa::with('camposAutenticacao')->find(2);
        #$empresa = Empresa::withoutGlobalScopes()
        #->where('empresa_id', 1)->first();     
        foreach ($empresa->camposAutenticacao as $campo) {
            $campos[] = [
                'nome' => $campo->nome
            ];
        }
        $data = [
            'logo' => $empresa->logo,
            'camposAutenticacao' => $campos,
            'uuid' => $empresa->uuid
        ];

        return new EmpresaResource($data);
    }
}
