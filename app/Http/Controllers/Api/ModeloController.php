<?php

namespace App\Http\Controllers\Api;

use App\Models\Modelo;
use Illuminate\Http\Request;
use App\Exports\ModeloExport;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Resources\ModeloResource;
use App\Models\UsuarioTemp;
use Illuminate\Support\Facades\Config;

class ModeloController extends Controller
{

    protected $modelo;

    public function __construct(Modelo $modelo)
    {
        $this->modelo = $modelo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modelos = $this->modelo->paginate();
        return ModeloResource::collection($modelos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add([
            'usuario_id' => Auth::user()->id            
        ]);

        $modelo = $this->modelo->create($request->all());

        if ($request->hasFile('foto_frente') && $request->file('foto_frente')->isValid()) {
            $nome = "foto_frente";
            $extensao = $request->foto_frente->extension();
            $nomeImagem = "{$nome}.$extensao";            
            $upload = $request->foto_frente->storeAs("modelos/".$modelo->id, $nomeImagem);

            if (!$upload){
                return response()->json([
                    "erro" => "falaha no upload"
                ], 500);
            }else{
                $modelo = Modelo::find($modelo->id);
                $modelo->update([
                    'foto_frente' => $nomeImagem
                ]);
            }
        }

        if ($request->hasFile('foto_verso') && $request->file('foto_verso')->isValid()) {
            $nome = "foto_verso";
            $extensao = $request->foto_verso->extension();
            $nomeImagem = "{$nome}.$extensao";            
            $upload = $request->foto_verso->storeAs("modelos/".$modelo->id, $nomeImagem);

            if (!$upload){
                return response()->json([
                    "erro" => "falaha no upload"
                ], 500);
            } else {
                $modelo = Modelo::find($modelo->id);
                $modelo->update([
                    'foto_verso' => $nomeImagem
                ]);
            }
        }
        
        return new ModeloResource($modelo);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modelo = $this->modelo->find($id);
        return new ModeloResource($modelo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modelo = $this->modelo->find($id);
        $modelo->update($request->all());
        return new ModeloResource($modelo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modelo = $this->modelo->find($id);
        $modelo->delete();

        return response()->json([
            'mensagem' => "Registro excluido com sucesso"
        ], 200);
    }

    public function camposVariaveisModelo(Request $request){
        $modelo = Modelo::with('camposVariaveis')
        ->where('id', $request->modelo_id)
        ->get();
        return new ModeloResource($modelo);
    }

    public function exportarModelo(Request $request){
        
        Excel::store(new ModeloExport($request->modelo_id), 'modelos/exportar/modelo_'. $request->modelo_id.'.xlsx');
                
        $modelo = [
            'link' => URL::to("storage/empresas/". session()->get('empresa.uuid')."/modelos/exportar/modelo_" .$request->modelo_id . '.xlsx')
        ];
        return new ModeloResource($modelo);
    }

    public function selecionarModelo(Request $request){

        $modelo = $this->modelo->find($request->modelo_id);

        if ($modelo) {
            session([
                'modelo' => [
                    'modelo_id' => $modelo->id                    
                ]
            ]);

            return new ModeloResource($modelo);
        } else {
            return response()->json([
                'error' => "Modelo não encotrado"
            ], 404);
        }
    }
}
