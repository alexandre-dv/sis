<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Status;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\HistoricoRemessa;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;


class CorreioController extends Controller
{
    protected $status, $historicoRemessa;

    public function __construct(Status $status, HistoricoRemessa $historicoRemessa)
    {
        $this->status = $status;
        $this->historicoRemessa = $historicoRemessa;
    }

    public function rastrearEncomendaCorreios(Request $request){

        #criar uma regra para verificação dos estatus da encomenda na api linetrack,para que toda vez que o usuario quiser acompanhar  em tempo real
        #não gere muitas solicitações na api,partindo do principio que o status da encomenda e atualizado uma vez por dia pelos correios
        #talvez criar um comando que verifica de hora em hora o status fazendo uma requisição na api e atualiza o status quando o mesmo for atualizado pelos correios
        #e quando for atualizado criar uma condicional verificando se existe um novo registro no dia de atualização de status,
        #desta forma o comando quando inserir o novo status para automaticamento de ficar fazendo requisição na api naquele dia.
        $client = new Client([
            'allow_redirects' => false,
            'cookies'         => true,
            'verify'          => false
        ]);
        $response = $client->request("GET", Config::get('correios.encomendas.url') . Config::get('correios.encomendas.token') . "&user=" . Config::get('correios.encomendas.usuario') . "&codigo=" . $request->codigo);    
        $encomenda = json_decode($response->getBody()->getContents());
        
        foreach ($encomenda->eventos as $item) {
           
            $data = Carbon::createFromFormat("d/m/Y H:i:s", $item->data.' '. $item->hora.':00')->format('Y-m-d H:i:s');
            
            //verifica se o status existe, se nao existir cria
            $status = Status::where('nome', $item->status)->first();
            if (!$status) {
                //1 correios 
                //2 remessa
               $status = Status::create([
                    'nome' => $item->status,
                    'tipo' => 1, //correios                    
                ]);
            }

            //verifica se o andamento da encomenda ainda não foi registrado
            $historico = HistoricoRemessa::where('data', $data)->first();
           
            if (!$historico) {
                HistoricoRemessa::firstOrCreate([
                    'remessa_id' => 1,
                    'status_id' => $status->id,
                    'data' => $data,
                ]);
            }
        }     
    }

    public function buscarCep(Request $request){

        try{

            $client = new Client([
                'allow_redirects' => false,
                'cookies'         => true,
                'verify'          => false
            ]);
            
            $response = $client->request("GET", Config::get('correios.cep.via-cep.url').$request->cep."/json/");
            
            $cep = json_decode($response->getBody()->getContents());
            
            return response()->json([
                'cep' => $cep->cep,
                'endereco' => $cep->logradouro,
                'complemento' => $cep->complemento,
                'bairro' => $cep->bairro,
                'cidade' => $cep->localidade,
                'estado' => $cep->uf,
                'ibge' => $cep->ibge,
                'gia' => $cep->gia,
                'ddd' => $cep->ddd,
                'siafi' => $cep->siafi,
                'mensagem' => "Cep encontrado"
            ], 200);
        }catch(\Exception $e) {
            return response()->json([
                'mensagem' => "Cep não encontrado"
            ], 500);
        }
    }
}
