<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Modelo;
use App\Models\Empresa;
use App\Models\UsuarioTemp;
use Illuminate\Http\Request;
use App\Models\CampoAutenticacao;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;
use Intervention\Image\Facades\Image;
use GuzzleHttp\Client as GuzzleClient;
use App\Http\Resources\UsuarioTempResource;
use Illuminate\Validation\ValidationException;

class UsuarioTempController extends Controller
{
    protected $usuarioTemp;
    protected $imageManager;

    public function __construct(UsuarioTemp $usuarioTemp, ImageManager $imageManager)
    {
        $this->usuarioTemp = $usuarioTemp;
        $this->imageManager = $imageManager;
    }

    public function autenticar(Request $request)
    {

        $request->request->add([
            'device_name' => $request->header('User-Agent')
        ]);

        $empresa = Empresa::where('uuid', $request->uuid)->first();
        $request->request->remove('uuid');

        $camposAutenticacao = CampoAutenticacao::where('empresa_id', $empresa->id)->get();
        foreach ($camposAutenticacao as $campo) {
            $condicionais[] = ["{$campo->nome}", '=', "{$request->{$campo->nome}}"];
        }

        $usuarioTemp = $this->usuarioTemp->where($condicionais)->first();

        Auth::guard('usuario_temp')->login($usuarioTemp);
        $usuario = UsuarioTemp::find(Auth::guard('usuario_temp')->user()->id);
        $usuario->update([
            'empresa_selecionada' => $empresa->id
        ]);

        if (!$usuarioTemp) {
            throw ValidationException::withMessages([
                'email' => ['As credenciais fornecidas estão incorretas.'],
            ]);
        }

        // Revogar todos os tokens.
        $usuarioTemp->tokens()->delete();

        activity('Login')->by($usuarioTemp)
            ->withProperties($condicionais)
            ->log('login realizado com sucesso');

        return (new UsuarioTempResource($usuarioTemp))
            ->additional([
                'token' => $usuarioTemp->createToken($request->header('User-Agent'))->plainTextToken,
                //'teste' => session()->get('empresa.empresa_id'),
                //'guard' => Auth::guard('usuario_temp')->user()
            ]);
    }

    public function uploadImagemCrop(Request $request){

        if ($request->hasFile('imagem_crop') && $request->file('imagem_crop')->isValid()) {
            try {
                Image::make($request->file('imagem_crop'))->encode('jpg', 100)->save(storage_path() . "/app/public/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_crop/" . Auth::guard()->user()->id . '.jpg');
                $usuario = UsuarioTemp::find(Auth::guard()->user()->id);
                $usuario->update([
                    'etapa' => 7,
                    'ultima_etapa' => 7
                ]);
                return new UsuarioTempResource($usuario);
            } catch (\Exception $e) {
                return response()->json([
                    'erro' => $e->getMessage(),
                    'linha' => $e->getLine()
                ], 500);
            }
        }        
    }

    public function uploadImagemUsuario(Request $request){

        if ($request->hasFile('imagem_usuario') && $request->file('imagem_usuario')->isValid()) {
            try {
                Image::make($request->file('imagem_usuario'))->encode('jpg', 100)->save(storage_path() . "/app/public/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_original/" . Auth::guard()->user()->id . '.jpg');
                $usuario = UsuarioTemp::find(Auth::guard()->user()->id);
                $usuario->update([
                    'etapa' => 6,
                    'ultima_etapa' => 6
                ]);
                return new UsuarioTempResource($usuario);
            } catch (\Exception $e) {
                return response()->json([
                    'erro' => $e->getMessage(),
                    'linha' => $e->getLine()
                ], 500);
            }
        }
    }

    public function carregaUsuario(){

        $usuario = UsuarioTemp::find(Auth::guard()->user()->id);
        $modelo = Modelo::find(Auth::guard()->user()->modelo_id);
        $empresa = Empresa::find(Auth::guard()->user()->empresa_id);
        $configEmpresa = [
            'logo' => $empresa->logo,
            'tema' => $empresa->tema
        ];

        if ($usuario->foto_aprovada == 1) {
            if ($modelo->merge_fundo_foto == 1) {
                $usuario['foto_usuario_aprovada'] = URL::to("storage/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_remove_bg/" . Auth::guard()->user()->id . '.jpg?id=' . mt_rand(5, 15));
            } else {
                $usuario['foto_usuario_aprovada'] = URL::to("storage/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_crop/" . Auth::guard()->user()->id . '.png?id=' . mt_rand(5, 15));
            }
        } elseif ($usuario->foto_reprovada == 1) {
            if ($modelo->merge_fundo_foto == 1) {
                $usuario['foto_usuario_reprovada'] = URL::to("storage/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_remove_bg/" . Auth::guard()->user()->id . '.jpg?id=' . mt_rand(5, 15));
            } else {
                $usuario['foto_usuario_reprovada'] = URL::to("storage/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_crop/" . Auth::guard()->user()->id . '.png?id=' . mt_rand(5, 15));
            }
        } else {
            if ($modelo->merge_fundo_foto == 1) {
                $usuario['foto_aguardando_aprovacao'] = URL::to("storage/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_remove_bg/" . Auth::guard()->user()->id . '.jpg?id=' . mt_rand(5, 15));
            } else {
                $usuario['foto_aguardando_aprovacao'] = URL::to("storage/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_crop/" . Auth::guard()->user()->id . '.png?id=' . mt_rand(5, 15));
            }
        }
        return new UsuarioTempResource($usuario);
    }

    public function listaNomes()
    {
        $usuario = Auth::guard()->user();
        $exploded = explode(" ", $usuario->nome_completo);
        $nomeSelecionado = [];
        foreach ($exploded as $key => $value) {
            $nomeSelecionado[] = [
                'selecionado' => $key == 0 ? 1 : 0,
                'nome'     => $value
            ];
        }

        return new UsuarioTempResource($nomeSelecionado);
    }

    public function confirmarNome(Request $request)
    {
        $i = 0;
        $nomeSelecionado = "";
        if ($request->nomes && count($request->nomes)) {
            foreach ($request->nomes as $key => $n) {
                if ($n['selecionado'] == 1) {
                    $nomeSelecionado .= $n['nome'] . " ";
                    $i++;
                }
            }
        }

        #criar uma tabela chamada config_empresa_temp para guardar as configurações da empresa relacionada ao usuario temp
        #para assim poder definir quais da emresa regras irão funcionar para o usuario durante as etpas
        #exemplo : pode ter ou não email poder ter ou não apelido pode ou não selecionar o nome ou vim fixo
        #poder ou não mostrar o modal depois de logado para o usuario escolher a ficha tecnica
        $usuario = UsuarioTemp::find(Auth::guard()->user()->id);
        $usuario->nome_impressao = trim($nomeSelecionado);
        $usuario->email          = trim($request->parametros['email']);
        $usuario->apelido        = trim($request->parametros['apelido']);
        $usuario->etapa          = 3;
        $usuario->ultima_etapa      = $usuario->ultima_etapa > $usuario->etapa ? $usuario->ultima_etapa : $usuario->etapa;
        $usuario->save();
        return new UsuarioTempResource($usuario);
    }

    public function mudarEtapa(Request $request)
{

        $usuario = UsuarioTemp::find(Auth::guard()->user()->id);

        if ($request->etapa == 4) {
            $usuario->update([
                'data_aceite_termo_1' => Carbon::now()
            ]);
        }

        if ($request->etapa == 5) {
            $usuario->update([
                'data_aceite_termo_2' => Carbon::now()
            ]);
        }
        //etapa atual maior que a ultima etapa
        if ($request->etapa > $usuario->ultima_etapa && $request->etapa != 0) {
            $usuario->update([
                'etapa' => $request->etapa,
                'ultima_etapa' => $request->etapa
            ]);
        } elseif($request->etapa != 0) {
            $usuario->update([
                'etapa' => $request->etapa
            ]);
        }

        //if($usuario->etapa == 1){}

        return new UsuarioTempResource($usuario);
    }

    public function carregaImagemOriginal(Request $request)
    {
        if (Auth::guard()->check()) {
            header("Content-type: image/jpg");
            header('Access-Control-Allow-Origin: http://app.portal.local:4200');
            header('Access-Control-Allow-Credentials: true');
            echo file_get_contents(storage_path() . "/app/public/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_original/" . Auth::guard()->user()->id . '.jpg');
        } else {
            return false;
        }
    }

    public function removerFundoImagem(){
        try {

            $modelo = Modelo::find(Auth::guard()->user()->modelo_id);

            #Remove BG
            if ($modelo->remove_bg) {
                #Faz uma copia da foto que esta na pasta fotos_usuarios_crop para a pasta fotos_usuarios_remove_bg
                #depois envia a foto que foi copiada para a pasta fotos_usuarios_remove_bg para remover o fundo
                /*
                $empresa = Auth::guard()->user()->empresaSelecionada();
                $fileCrop = realpath(storage_path("app/public/empresas/" . $empresa->uuid . "/usuarios_temp/fotos_usuarios_crop/" . Auth::guard()->user()->id . ".jpg"));
                $fileRemoveBg = storage_path("app/public/empresas/" . $empresa->uuid . "/usuarios_temp/fotos_usuarios_remove_bg/" . Auth::guard()->user()->id . ".png");
                File::copy($fileCrop, $fileRemoveBg);

                $client = new GuzzleClient();
                $response = $client->post('https://api.remove.bg/v1.0/removebg', [
                    'multipart' => [
                        [
                            'name'     => 'image_file',
                            'contents' => fopen($fileRemoveBg, 'r')
                        ],
                        [
                            'name'     => 'size',
                            'contents' => 'medium'
                        ]
                    ],
                    'headers' => [
                        'X-Api-Key' => env('REMOVE_BG_API_KEY')
                    ]
                ]);

                $fp = fopen($fileRemoveBg, "wb");
                fwrite($fp, $response->getBody());
                fclose($fp);
                */
            }

            #Merge
            if ($modelo->merge_fundo_foto) {
                $img_pessoa = storage_path() . "/app/public/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_remove_bg/" . Auth::guard()->user()->id . '.png';
                $img_background = storage_path() . "/app/public/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fundos_merge/fundo_merge_" . Auth::guard()->user()->modelo_id . '.jpg';

                $img_canvas = $this->imageManager->canvas(800, 1249);

                $img_canvas->insert($this->imageManager->make($img_background), 'center');
                $img_canvas->insert($this->imageManager->make($img_pessoa), 'center');

                $img_canvas->save(storage_path() . "/app/public/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_remove_bg/" . Auth::guard()->user()->modelo_id . '.jpg', 100);
            }
            #Merge

            $usuario = UsuarioTemp::whereId(Auth::guard()->user()->id)->first();

            $usuario->etapa = 8;
            $usuario->ultima_etapa = 8;
            $usuario->ultima_etapa = $usuario->ultima_etapa > $usuario->etapa ? $usuario->ultima_etapa : $usuario->etapa;
            $usuario->total_remove_bg += 1;
            $usuario->save();
            $usuario = UsuarioTemp::find(Auth::guard()->user()->id);
            return new UsuarioTempResource($usuario);
        } catch (\Exception $e) {
            return response()->json([
                'erro' => $e->getMessage(),
                'linha' => $e->getLine()
            ], 500);
        }
    }

    public function refazerSolicitacao(){

        #Não pode limpar os dados que o usuario faz o login neste caso o cpf ea data de nascimento
        $usuario = UsuarioTemp::find(Auth::guard()->user()->id);
        $usuario->update([
            'etapa' => 1,
            'ultima_etapa' => 1,
            'data_aceite_1' => null,
            'data_aceite_2' => null,
            'data_aprovacao' => null,
            'data_reprovacao' => null,
            'motivo_reprovacao' => null,
            'foto_aprovada' => null,
            'foto_reprovada' => null,
            'data_solicitacao' => null,
            'refazer_solicitacao' => null,
            'email' => null,
            'matricula' => null,
            'nome_impressao' => null,
            'apelido' => null,
            'data_aceite_termo_1' => null,
            'data_aceite_termo_2' => null,
        ]);

        return new UsuarioTempResource($usuario);
    }

    public function configuracaoModelo()
    {
        $modelo = Modelo::find(Auth::guard()->user()->modelo_id);
        $data = [
            'id' => $modelo->id,
            'fundo_transparente' => $modelo->fundo_transparente,
            'custom_css_cracha' => $modelo->custom_css_cracha,
            'fundo_transparente_frente' => $modelo->fundo_transparente_frente,
            'fundo_transparente_verso' => $modelo->fundo_transparente_verso,
            'layout_cracha_customizado' => $modelo->layout_cracha_customizado,
            'layout_cracha_info' => $modelo->layout_cracha_info,
            'proporcao_crop' => $modelo->proporcao_crop
        ];

        return new UsuarioTempResource($data);
    }

    public function finalizarEnvio()
    {
        $usuario = UsuarioTemp::find(Auth::guard()->user()->id);
        $usuario->update([
            'etapa' => 9,
            'ultima_etapa' => 9,
            'solicitacao_enviada' => 1,
            'data_solicitacao' => Carbon::now()
        ]);

        $modelo = Modelo::find(Auth::guard()->user()->modelo_id);

        if ($modelo->merge_fundo_foto == 1) {
            $usuario['foto_aguardando_aprovacao'] = URL::to("storage/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_remove_bg/" . Auth::guard()->user()->id . '.jpg?id=' . mt_rand(5, 15));
        } else {
            $usuario['foto_aguardando_aprovacao'] = URL::to("storage/empresas/" . Auth::guard()->user()->empresaSelecionada()->uuid . "/usuarios_temp/fotos_usuarios_crop/" . Auth::guard()->user()->id . '.png?id=' . mt_rand(5, 15));
        }

        return new UsuarioTempResource($usuario);
    }
}
