<?php

namespace App\Http\Controllers\Api;

use ZipArchive;
use App\Models\Modelo;
use App\Models\Remessa;
use App\Models\Solicitacao;
use Illuminate\Http\Request;
use App\Models\CampoVariavel;
use App\Exports\SolicitacaoExport;
use App\Imports\SolicitacaoImport;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Resources\SolicitacaoResource;

class SolicitacaoController extends Controller{

    protected $solicitacao;
    protected $remessa;

    public function __construct(Solicitacao $solicitacao, Remessa $remessa)
    {
        $this->solicitacao = $solicitacao;
        $this->remessa = $remessa;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitacoes = $this->solicitacao->paginate();
        return SolicitacaoResource::collection($solicitacoes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add([
            'usuario_id' => Auth::user()->id            
        ]);

        $solicitacao = $this->solicitacao->create($request->all());
        $camposvariaveis = CampoVariavel::where('modelo_id', $request->modelo_id)->get();
    
        foreach($camposvariaveis as $campoVariavel){
            $solicitacao->camposVariaveis()->attach($campoVariavel->id, [
                'valor' => $request->{$campoVariavel->nome_tecnico},
            ]);
        }

       return new SolicitacaoResource($solicitacao);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $solicitacao = $this->solicitacao->find($id);
        return new SolicitacaoResource($solicitacao);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $solicitacao = $this->solicitacao->find($id);
        $solicitacao->update($request->all());
        return new SolicitacaoResource($solicitacao);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $solicitacao = $this->solicitacao->find($id);
        $solicitacao->delete();

        return response()->json([
            'mensagem' => "Solicitacao excluida com sucesso"
        ], 200);
    }

    public function exportarSolicitacoes(Request $request){

        #pode baixar todas as solicitacoes de uma remessa especifica
        #pode baixar todas as solicitacoes de um modelo especifico
        $arquivo_solicitacoes = Auth::user()->id.'_'.date('Y-m-d');

        if(isset($request->modelo_id)){
            $remessas = Remessa::where('modelo_id', $request->modelo_id)->get();
            foreach ($remessas as $remessa) {
                Excel::store(new SolicitacaoExport($remessa), 'remessas/exportar/solicitacoes_' . $arquivo_solicitacoes . '.xlsx');
            }
        }elseif(isset($request->remessa_id)){
            $remessa = Remessa::find($request->remessa_id);
            Excel::store(new SolicitacaoExport($remessa), 'remessas/exportar/remessa_' . $arquivo_solicitacoes . '.xlsx');
        }
        
        //criar condicional para verificar se apasta download existe se nao cria
        $zip = new ZipArchive();
        $file_path = base_path("storage/app/public/empresas/".session()->get('empresa.uuid')."/downloads/".date('Y-m-d')."_solicitacoes.zip");
        $zip->open($file_path, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        $pasta = date('Y-m-d');
        $filePlanilha = storage_path("app/public/empresas/" . session()->get('empresa.uuid') . "/remessas/exportar/solicitacoes_" . $arquivo_solicitacoes . ".xlsx");
        
        if (isset($request->modelo_id)) {
            foreach ($remessas as $remessa) {
                foreach ($remessa->solicitacoes as $solicitacao) {
                    $file = storage_path("app/public/empresas/" . session()->get('empresa.uuid') . "/remessas/" . $remessa->id . "/solicitacoes/{$solicitacao->foto_aprovada}");
                    if (file_exists($file) && is_file($file)) {
                        $zip->addFile($file, $pasta . '/' . $solicitacao->foto_aprovada);
                    }
                }
            }

            if (file_exists($filePlanilha) && is_file($filePlanilha)) {            
                $zip->addFile($filePlanilha, $pasta . '/' . "cliente.xlsx");
            }
        } elseif (isset($request->remessa_id)) {

            $solicitacoes = Solicitacao::where('remessa_id', $request->remessa_id)->get();
            foreach ($solicitacoes as $solicitacao) {
                $file = storage_path("app/public/empresas/" . session()->get('empresa.uuid') . "/remessas/" . $remessa->id . "/solicitacoes/{$solicitacao->foto_aprovada}");                
                if (file_exists($file) && is_file($file)) {
                    $zip->addFile($file, $pasta . '/' . $solicitacao->foto_aprovada);                    
                }
            }           
            if (file_exists($filePlanilha) && is_file($filePlanilha)) {            
                $zip->addFile($filePlanilha, $pasta . '/' . "cliente.xlsx");
            }
        }

        $zip->close();

        $solicitacao = [
            'link_execel' => URL::to("storage/empresas/". session()->get('empresa.uuid')."/remessas/exportar/solicitacoes_" . $arquivo_solicitacoes . '.xlsx'),
            'link_zip' => URL::to("storage/empresas/" . session()->get('empresa.uuid') . "/downloads/".date('Y-m-d')."_solicitacoes.zip")
        ];
        return new SolicitacaoResource($solicitacao);
    }

    public function importarSolicitacoes(Request $request){

        $request->request->add([
            'usuario_id' => Auth::user()->id,
            'status' => 0,
            'modelo_id' =>  session()->get('modelo.modelo_id')
        ]);

        $remessa = $this->remessa->create($request->all());

        try {            
            $import = new SolicitacaoImport($remessa);
            Excel::import($import, request()->file('file'));

            return response()->json(
                [
                    'sucesso' => "Planilha importada com sucesso"
                ],
                200
            );


        } catch (\Exception $e) {

            $remessa = Remessa::find($remessa->id);
            $remessa->forceDelete();

            return response()->json(
                [
                    'error' => $e->getMessage()
                ],
                500
            );
        }
    }
}
