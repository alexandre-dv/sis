<?php

namespace App\Imports;

use App\Models\Etiqueta;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EtiquetasImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public $data = [];
    public function model(array $row)
    {       
        $data = Etiqueta::create([
            'codigo' => $row['codigo']
        ]);

        $this->data[] = [
            'codigo' => $data->codigo
         ];
    }

    public function etiquetas()
    {
        return $this->data;
    }
}
