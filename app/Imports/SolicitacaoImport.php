<?php

namespace App\Imports;

use App\Models\Remessa;
use App\Models\Solicitacao;
use App\Models\CampoVariavel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SolicitacaoImport implements ToModel, WithHeadingRow{

    protected $remessa;

    function __construct(Remessa $remessa){

        $this->remessa = $remessa;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row){
        
        $camposvariaveis = CampoVariavel::where('modelo_id', $this->remessa->modelo_id)
        ->where('presente_criar_solicitacao', 1)
        ->get();
        $solicitacao = Solicitacao::create([
            'remessa_id' => $this->remessa->id           
        ]);
        
        foreach ($camposvariaveis as $campoVariavel) {

            $solicitacao->camposVariaveis()->attach($campoVariavel->id, [
                'valor' => $row[$campoVariavel->nome_tecnico],
            ]);
        }        
    }
}
