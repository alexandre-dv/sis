<?php

namespace App\Exports;

use App\Models\CampoVariavel;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ModeloExport implements FromCollection, WithHeadings, WithStyles
{

    protected $modelo_id;

    function __construct($modelo_id)
    {
        $this->modelo_id = $modelo_id;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect();
    }


    /**
     * Write code on Method
     *
     * @return response()

     */

    public function headings(): array
    {
        $campos = CampoVariavel::where('presente_planilha_modelo', 1)
        ->where('modelo_id', $this->modelo_id)
        ->get();

        $data = [];
        foreach($campos as $campo){
            $data[] = $campo->nome_tecnico;
        }
        
       return $data;
    }

    public function styles(Worksheet $sheet){
        
        $sheet->getStyle('A1:N1')
        ->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('51d2b7');

        $sheet->getStyle('A1:N1')
        ->getFont()
            ->setBold(true)
            ->getColor()
            ->setRGB('00000');
    }
}
