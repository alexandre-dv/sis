<?php

namespace App\Exports;

use App\Models\Modelo;
use App\Models\Remessa;
use App\Models\Solicitacao;
use App\Models\CampoVariavel;
use Illuminate\Support\Collection;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SolicitacaoExport implements FromCollection, WithHeadings, WithStyles
{

    protected $remessa;

    function __construct(Remessa $remessa)
    {
        $this->remessa = $remessa;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection(){

        $camposVariaveis = CampoVariavel::where('modelo_id', $this->remessa->modelo_id)->pluck('id');

        $with = ['camposVariaveis' => function($q) use($camposVariaveis){
            $q->whereIn('campo_variavel_id', $camposVariaveis);
        }];

        $solicitacoes = Solicitacao::with($with)->where('remessa_id', $this->remessa->id)->get();
        $scv_solicitacoes = [];
        $scv_valores = [];        
        foreach($solicitacoes as $solicitacao){
            foreach($solicitacao->camposVariaveis as $campo){
                $scv_valores[] = $campo->pivot->valor;                              
            }

            $scv_valores[] = $solicitacao->foto_aprovada;
            $scv_solicitacoes[] = $scv_valores;           
            $scv_valores = [];
        }

        return new Collection(
            $scv_solicitacoes
        );
    }

    public function headings(): array
    {
        
        $campos = CampoVariavel::where('presente_planilha_modelo', 1)
        ->where('modelo_id', $this->remessa->modelo_id)
        ->get();

        $data = [];
        foreach($campos as $campo){
            $data[] = $campo->nome_tecnico;
        }

        $data[] = 'foto';
       return $data;
    }

    public function styles(Worksheet $sheet){
        
        $sheet->getStyle('A1:N1')
        ->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('51d2b7');

        $sheet->getStyle('A1:N1')
        ->getFont()
            ->setBold(true)
            ->getColor()
            ->setRGB('00000');
    }
}
