<?php


namespace App\Scopes\Empresa;

use App\Empresa\GerenciarEmpresa;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Auth;

class EmpresaScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if (Auth::check()) {            
            $empresa = app(GerenciarEmpresa::class)->getEmpresaIdentify();
            $builder->where($model->getTable() . '.empresa_id', $empresa);
        }
    }
}