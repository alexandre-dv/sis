<?php

namespace App\Empresa;

use App\Models\Empresa;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class GerenciarEmpresa
{

    public function getEmpresaIdentify()
    {
        return $this->getEmpresa()->id;
    }

    public function getEmpresa(){
        try {            
            return Auth::guard()->user()->empresaSelecionada();            
        }catch(\Exception $e) {
            return response()->json([
                'mensagem' => $e->getMessage()
            ], 500);
        }
    }
}
