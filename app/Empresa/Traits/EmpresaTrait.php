<?php


namespace App\Empresa\Traits;


use App\Observers\Empresa\EmpresaObserver;
use App\Scopes\Empresa\EmpresaScope;

trait EmpresaTrait
{
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new EmpresaScope());
        static::observe(new EmpresaObserver());
    }
}
