<?php

namespace App\Models;

use App\Empresa\Traits\EmpresaTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Etiqueta extends Model
{
    use HasFactory, SoftDeletes, LogsActivity, EmpresaTrait;
    
    protected $table = 'etiquetas';
 
    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = ['codigo', 'empresa_id','remessa_id','exportado'];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'etiquetas';

    protected $fillable = [
        'codigo',
        'remessa_id',
        'exportado'        
    ];

    public function tipoEntrega()
    {
        return $this->belongsTo(TipoEntrega::class);
    }
}
