<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TipoEntrega extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'tipo_entregas';

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = ['nome'];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'tipo_entregas';

    protected $fillable = [
        'nome'       
    ];
}
