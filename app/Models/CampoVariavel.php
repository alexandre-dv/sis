<?php

namespace App\Models;

use App\Empresa\Traits\EmpresaTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CampoVariavel extends Model
{
    use HasFactory, SoftDeletes, LogsActivity, EmpresaTrait;

    protected $table = 'campos_variaveis';

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = [
        'campo_chave',
        'modelo_id',
        'nome_label', 
        'nome_tecnico', 
        'camel_case',
        'api', 
        'presente_planilha_modelo', 
        'presente_criar_solicitacao',
        'empresa_id'
    ];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'campos_variaveis';

    protected $fillable = [
        'campo_chave',
        'modelo_id',
        'nome_label', 
        'nome_tecnico', 
        'camel_case',
        'api', 
        'presente_planilha_modelo', 
        'presente_criar_solicitacao'
    ];

    public function solicitacoes()
    {
        return $this->hasMany(Solicitacao::class);
    }

    public function solicitacoesCamposVariaveis(){
        
        return $this->belongsToMany(Solicitacao::class, "solicitacoes_campos_variaveis")
        ->withTimestamps()
        ->withPivot(['valor']);
    }
}
