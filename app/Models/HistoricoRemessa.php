<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HistoricoRemessa extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'historico_remessa';

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = [
        'status_id',   
        'remessa_id',     
        'usuario_id',
        'data'
    ];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'historico_remessas';

    protected $fillable = [
        'status_id',   
        'remessa_id',     
        'usuario_id',
        'data'
    ];

    public function remessa()
    {
        return $this->belongsTo(Remessa::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }
}
