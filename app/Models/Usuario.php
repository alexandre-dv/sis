<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, LogsActivity;

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = ['nome','email', 'cpf', 'empresa_selecionada'];

    #o evento de alteração de senha não será registrado
    protected static $ignoreChangedAttributes = ['password','updated_at', 'foto_perfil'];
    
    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created','updated','deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true; 

    #customizar o nome do log
    protected static $logName = 'usuarios';

    /*public function getDescriptionForEvent(string $eventName){

        switch ($eventName) {
            case "created":
                return "Criação de usuário";
                break;
            case "updated":
                return "Atualização do usuário";
                break;
            case "deleted":
                return "Exclusão do usuário";
                break;
        }
    }*/

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'usuarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'email',
        'cpf',
        'foto_perfil',
        'password',
        'uuid',
        'empresa_selecionada'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'foto_usuario',
        'foto_aprovada',     
    ];

    public function papeis()
    {
        return $this->belongsToMany(Papel::class);
    }

    public function usuariosPapeis()
    {
        return $this->belongsToMany(Papel::class, 'papel_usuario')->withTimestamps();
    }

    public function empresasUsuarios()
    {
        return $this->belongsToMany(Empresa::class, 'empresa_usuario')->withTimestamps();
    }

    public function isAdmin()
    {
        return $this->existePapel('Administrador');
    }

    public function existePapel($papel)
    {
        if (is_string($papel)) {
            $papel = Papel::where('nome', $papel)->firstOrFail();
        }

        return (bool) $this->papeis()->find($papel->id);
    }

    public function adicionaPapel($papel)
    {
        if (is_string($papel)) {
            $papel = Papel::where('nome', '=', $papel)->firstOrFail();
        }

        if ($this->existePapel($papel)) {
            return;
        }

        return $this->papeis()->attach($papel);
    }

    public function temUmPapelDestes($papeis)
    {
        $usuarioPapeis = $this->papeis;
        return $papeis->intersect($usuarioPapeis)->count();
    }
    
    public function getFotoUsuarioAttribute(){
        if (Auth::check()) {
            #return storage_path("storage/empresas/".Request::session()->get('empresa.uuid')."usuarios_temp/") . $this->getAttribute('id') . '.jpg';            
            return URL::to("storage/empresas/" .Auth::guard()->user()->empresaSelecionada()->uuid."/usuarios_temp/fotos_usuarios_original/".$this->getAttribute("id").'.jpg');
        }else{
            return null;
        }
    }

    public function getFotoAprovadaAttribute(){
        if (Request::session()->has('empresa')) {
            #return storage_path("storage/empresas/".Request::session()->get('empresa.uuid')."usuarios_temp/") . $this->getAttribute('id') . '.jpg';
            return URL::to("storage/empresas/" .Request::session()->get('empresa.uuid')."/usuarios_temp/fotos_aprovadas/".$this->getAttribute("id").'.jpg');
        }else{
            return null;
        }
    }

    public function empresaSelecionada(){
        $empresa = Empresa::find(Auth::guard()->user()->empresa_selecionada);
        return $empresa;
    }
}