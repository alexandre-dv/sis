<?php

namespace App\Models;

use App\Models\TipoEntrega;
use Illuminate\Support\Facades\URL;
use App\Empresa\Traits\EmpresaTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Modelo extends Model
{
    use HasFactory, SoftDeletes, LogsActivity, EmpresaTrait;

    protected $table = 'modelos';

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = [
        'nome', 
        'fundo_transparente',       
        'proporcao_crop',
        'dados',
        'foto',
        'furo',        
        'aprovado',
        'impressao_termica',
        'impressao_externa',
        'tipo_entrega_id',
        'link_projeto',
        'produto_id',
        'usuario_id',
        'empresa_id',               
        'layout_cracha_customizado',        
        'merge_fundo_foto',
        'layout_cracha_info'
    ];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'modelos';

    protected $fillable = [
        'nome', 
        'fundo_transparente',
        'proporcao_crop',
        'dados',
        'foto',
        'furo',        
        'impressao_termica',
        'impressao_externa',
        'link_projeto',
        'produto_id',
        'tipo_entrega_id',
        'aprovado',
        'usuario_id',
        'empresa_id',        
        'layout_cracha_customizado',
        'merge_fundo_foto',
        'remove_bg',
        'custom_css_cracha',
        'layout_cracha_info'
    ];

    protected $appends = [        
        'fundo_transparente_frente',
        'fundo_transparente_verso',        
    ];

    public function tipoEntrega()
    {
        return $this->belongsTo(TipoEntrega::class);
    }

    public function produto()
    {
        return $this->belongsTo(Produto::class);
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }

    public function camposVariaveis()
    {
        return $this->hasMany(CampoVariavel::class);
    }

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function getFundoTransparenteFrenteAttribute(){        
        if (Auth::guard()->check()) {                     
            if($this->getAttribute("fundo_transparente")){                
                return URL::to("storage/empresas/" .Auth::guard()->user()->empresaSelecionada()->uuid."/usuarios_temp/fundos_transparentes/fundo_transparente_frente_".$this->getAttribute("id").'.png?id='.mt_rand(5, 15));
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public function getFundoTransparenteVersoAttribute(){
        if (Auth::guard()->check()) {                     
            if($this->getAttribute("fundo_transparente")){                
                return URL::to("storage/empresas/" .Auth::guard()->user()->empresaSelecionada()->uuid."/usuarios_temp/fundos_transparentes/fundo_transparente_verso_".$this->getAttribute("id").'.jpg?id='.mt_rand(5, 15));
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
}
