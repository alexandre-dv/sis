<?php

namespace App\Models;

use App\Empresa\Traits\EmpresaTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory, SoftDeletes, EmpresaTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'posts';

    protected $fillable = [
        'titulo',
        'descricao'
    ];
}
