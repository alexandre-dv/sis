<?php

namespace App\Models;

use App\Empresa\Traits\EmpresaTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CampoAutenticacao extends Model
{
    use HasFactory, LogsActivity, EmpresaTrait;
    protected $table = 'campos_autenticacao';

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = ['nome','empresa_id'];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'campos_autenticacao';

    protected $fillable = [        
        'nome',
        'empresa_id'
    ];
}
