<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Papel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'papeis';

    protected $fillable = ['nome','descricao'];

    public function usuarios()
    {
        return $this->belongsToMany(Usuario::class);
    }

    public function permissoes()
    {
        return $this->belongsToMany(Permissao::class);
    }

    public function existePermissao($permissao)
    {
        if (is_string($permissao)) {
            $permissao = Permissao::where('nome', $permissao)->firstOrFail();
        }

        return (boolean) $this->permissoes()->find($permissao->id);

    }
    public function removePermissao($permissao)
    {
        if (is_string($permissao)) {
            $permissao = Permissao::where('nome','=',$permissao)->firstOrFail();
        }

        return $this->permissoes()->detach($permissao);
    }

    public function papeisUsuarios()
    {
        return $this->belongsToMany(Usuario::class,'papel_usuario')->withTimestamps();
    }
}
