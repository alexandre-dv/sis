<?php

namespace App\Models;

use App\Empresa\Traits\EmpresaTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CentroCusto extends Model
{
    use HasFactory, EmpresaTrait, LogsActivity, SoftDeletes;

    protected $table = 'centro_custos';

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = ['nome','codigo', 'descricao', 'empresa_id'];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'centro_custos';

    protected $fillable = [
        'nome',
        'codigo',
        'descricao'       
    ];
}
