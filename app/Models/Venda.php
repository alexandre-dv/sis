<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Venda extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'vendas';

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = ['produto_id', 'valor', 'desconto','tipo_desconto','quantidade','usuario_id','empresa_id'];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'vendas';

    protected $fillable = [
        'produto_id', 
        'valor', 
        'desconto', 
        'tipo_desconto', 
        'quantidade', 
        'usuario_id', 
        'empresa_id'
    ];

    public function produto()
    {
        return $this->belongsTo(Produto::class);
    }
}
