<?php

namespace App\Models;

use App\Empresa\Traits\EmpresaTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Remessa extends Model
{
    use HasFactory, SoftDeletes, LogsActivity, EmpresaTrait;

    protected $table = 'remessas';

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = [
        'status', //status zero é em criação
        'etapa',        
        'prazo_entrega',
        'aprovacao_fotos',
        'usuario_id',
        'modelo_id',
        'endereco_id',
        'empresa_id',
        //'segunda_via' controlar a segunda via pelo campo via na solicitação
    ];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'remessas';

    protected $fillable = [
        'status', 
        'etapa',         
        'segunda_via', 
        'prazo_entrega', 
        'aprovacao_fotos',
        'usuario_id',
        'modelo_id',
        'endereco_id', 
        'empresa_id'
    ];

    public function solicitacoes()
    {
        return $this->hasMany(Solicitacao::class);
    }
}
