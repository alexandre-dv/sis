<?php

namespace App\Models;

use App\Empresa\Traits\EmpresaTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Solicitacao extends Model
{
    use HasFactory, SoftDeletes, LogsActivity, EmpresaTrait;

    protected $table = 'solicitacoes';

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = [
        //'campo_chave',
        'remessa_id',
        'via',
        'codigo_chip',
        'foto_aprovacao',
        'foto_aprovada',
        'foto_reprovada',
        'motivo_reprovacao',
        //'usuario_id', o usuario que solicita esta na remessa       
        'status' //foto cortada - fundo retirado
    ];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'solicitacoes';

    protected $fillable = [
        //'campo_chave',
        'remessa_id',
        'via',
        'codigo_chip',
        'foto_aprovacao',
        'foto_aprovada',
        'foto_reprovada',
        'motivo_reprovacao',
        'usuario_id',
        'status' //foto cortada - fundo retirado
    ];

    public function camposVariaveis(){
        
        return $this->belongsToMany(CampoVariavel::class, "solicitacoes_campos_variaveis")
        ->withTimestamps()
        ->withPivot(['valor']);
    }
}
