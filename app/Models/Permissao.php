<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permissao extends Model
{
    use HasFactory;

    protected $table = 'permissoes';
    protected $fillable = ['nome','descricao'];

    public function papeis()
    {
        return $this->belongsToMany(Papel::class);
    }

    public function permissoesPapeis()
    {
        return $this->belongsToMany(Papel::class,'papel_permissao')->withTimestamps();
    }
}
