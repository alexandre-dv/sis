<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Produto extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'produtos';
    
    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = ['nome', 'tipo_produto_id'];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'produtos';

    protected $fillable = [
        'nome',
        'tipo_produto_id'
    ];

    public function tipoProduto()
    {
        return $this->belongsTo(TipoProduto::class);
    }
}
