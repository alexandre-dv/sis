<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Empresa extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    #atributos do model que terão os eventos registrados
    protected static $logAttributes = ['*'];

    #o evento de alteração de senha não será registrado
    #protected static $ignoreChangedAttributes = [];

    #os eventos [created,updated,deleted] será registrado automaticamente.
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'Empresas';

    protected $table = 'empresas';

    protected $fillable = [      
        'id', 
        'telefone',
        'email',
        'razao_social',
        'nome_fantasia',
        'inscricao_municipal',
        'inscricao_estadual',
        'cnpj',
        'cep',
        'endereco',
        'numero',
        'complemento',
        'bairro',
        'cidade',
        'estado',
        'logo',
        'uuid',
        'tema'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid(4);
        });
    }

    public function usuariosEmpresas()
    {
        return $this->belongsToMany(Usuario::class, 'empresa_usuario')->withTimestamps();
    }

    public function enderecos()
    {
        return $this->hasMany(Endereco::class);
    }

    public function camposAutenticacao()
    {
        return $this->hasMany(CampoAutenticacao::class);
    }
}
