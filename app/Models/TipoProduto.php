<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TipoProduto extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'tipos_produto';

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = ['nome', 'chip','tipo_chip'];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'tipos_produto';

    protected $fillable = [
        'nome',
        'tipo_produto_id'
    ];
}
