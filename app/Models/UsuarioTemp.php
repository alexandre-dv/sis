<?php

namespace App\Models;

use Carbon\Carbon;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UsuarioTemp extends Authenticatable
{
    use HasApiTokens, HasFactory, LogsActivity;

    #atributos do model que terão os eventos registrados 
    protected static $logAttributes = [
        'cpf', 
        'datanascimento', 
        'matricula',
        'email',
        'empresa_selecionada',
        'data_aprovacao',
        'data_reprovacao',
        'data_solicitacao',
        'refazer_solicitacao',
        'motivo_reprovacao',
        'solicitacao_enviada'
    ];

    #os eventos [created,updated,deleted] será registrado automaticamente.   
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    #registrar apenas os atributos alterados 
    protected static $logOnlyDirty = true;

    #customizar o nome do log
    protected static $logName = 'usuario_temp';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'usuario_temp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cpf',
        'email',
        'datanascimento',
        'matricula',
        'nome_completo',
        'nome_impressao',
        'apelido',
        'empresa_selecionada',        
        'etapa',
        'ultima_etapa',
        'data_aceite_termo_1',      
        'data_aceite_termo_2',
        'total_remove_bg',
        'modelo_id',
        'foto_aprovada',
        'foto_reprovada',
        'data_aprovacao',
        'data_reprovacao',
        'data_solicitacao',
        'refazer_solicitacao',
        'motivo_reprovacao',
        'solicitacao_enviada'       
    ];

    protected $appends = [
        'foto_usuario_crop',
        'foto_usuario_remove_bg',
        'foto_usuario_original',     
        'aceite_termo_1_br',
        'aceite_termo_2_br',
        'data_aprovacao_br',
        'data_reprovacao_br',
        'data_solicitacao_br'                  
    ];

    public function getAceiteTermo1BrAttribute(){
        if($this->getAttribute("data_aceite_termo_1")){
            return Carbon::createFromFormat("Y-m-d H:i:s", $this->getAttribute("data_aceite_termo_1"), 'America/Sao_Paulo')->format('d/m/Y H:i');
        }else{
            return null;
        }
    }

    public function getAceiteTermo2BrAttribute(){
        if($this->getAttribute("data_aceite_termo_2")){
            return Carbon::createFromFormat("Y-m-d H:i:s", $this->getAttribute("data_aceite_termo_2"), 'America/Sao_Paulo')->format('d/m/Y H:i');
        }else{
            return null;
        }
    }

    public function getFotoUsuarioCropAttribute(){
        if (Auth::guard()->check()) {            
            return URL::to("storage/empresas/" .Auth::guard()->user()->empresaSelecionada()->uuid."/usuarios_temp/fotos_usuarios_crop/".$this->getAttribute("id").'.jpg?id='.mt_rand(5, 15));            
        }else{
            return null;
        }
    }

    public function getFotoUsuarioRemoveBgAttribute(){
        if (Auth::guard()->check()) {
            $modelo = Modelo::find(Auth::guard()->user()->modelo_id);                        
            if($modelo->merge_fundo_foto){                    
                return URL::to("storage/empresas/" .Auth::guard()->user()->empresaSelecionada()->uuid."/usuarios_temp/fotos_usuarios_remove_bg/".$this->getAttribute("id").'.jpg?id='.mt_rand(5, 15));
            }else{
                return URL::to("storage/empresas/" .Auth::guard()->user()->empresaSelecionada()->uuid."/usuarios_temp/fotos_usuarios_remove_bg/".$this->getAttribute("id").'.png?id='.mt_rand(5, 15));
            }
        }else{
            return null;
        }
    }

    public function getFotoUsuarioOriginalAttribute(){
        if (Auth::guard()->check()) {         
            return URL::to("storage/empresas/" .Auth::guard()->user()->empresaSelecionada()->uuid."/usuarios_temp/fotos_usuarios_original/".$this->getAttribute("id").'.jpg?id='.mt_rand(5, 15));
        }else{
            return null;
        }
    }

    public function getDataAprovacaoBrAttribute(){
        if($this->getAttribute("data_aprovacao")){
            return Carbon::createFromFormat("Y-m-d H:i:s", $this->getAttribute("data_aprovacao"), 'America/Sao_Paulo')->format('d/m/Y H:i');
        }else{
            return null;
        }
    }

    public function getDataReprovacaoBrAttribute(){
        if($this->getAttribute("data_reprovacao")){
            return Carbon::createFromFormat("Y-m-d H:i:s", $this->getAttribute("data_reprovacao"), 'America/Sao_Paulo')->format('d/m/Y H:i');
        }else{
            return null;
        }
    }

    public function getDataSolicitacaoBrAttribute(){
        if($this->getAttribute("data_solicitacao")){
            return Carbon::createFromFormat("Y-m-d H:i:s", $this->getAttribute("data_solicitacao"), 'America/Sao_Paulo')->format('d/m/Y H:i');
        }else{
            return null;
        }
    }

    public function empresaSelecionada(){
        $empresa = Empresa::find(Auth::guard()->user()->empresa_selecionada);
        return $empresa;
    }
}
