<?php

namespace App\Observers\Empresa;

use App\Empresa\GerenciarEmpresa;
use Illuminate\Database\Eloquent\Model;

class EmpresaObserver
{
    public function creating(Model $model)
    {
        $empresa = app(GerenciarEmpresa::class)->getEmpresaIdentify();

        $model->setAttribute('empresa_id', $empresa);
    }
}
