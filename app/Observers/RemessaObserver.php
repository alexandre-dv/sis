<?php

namespace App\Observers;

use Carbon\Carbon;
use App\Models\Remessa;
use App\Models\HistoricoRemessa;
use Illuminate\Support\Facades\Auth;

class RemessaObserver
{
    /**
     * Handle the Remessa "created" event.
     *
     * @param  \App\Models\Remessa  $remessa
     * @return void
     */
    public function created(Remessa $remessa)
    {
        HistoricoRemessa::create([
            'remessa_id' => $remessa->id,
            'usuario_id' => Auth::user()->id,
            'status_id' => $remessa->status,
            'data' => Carbon::now()
        ]);
    }

    /**
     * Handle the Remessa "updated" event.
     *
     * @param  \App\Models\Remessa  $remessa
     * @return void
     */
    public function updated(Remessa $remessa)
    {
        HistoricoRemessa::create([
            'remessa_id' => $remessa->id,
            'usuario_id' => Auth::user()->id,
            'status_id' => $remessa->status,
            'data' => Carbon::now()
        ]);
    }

    /**
     * Handle the Remessa "deleted" event.
     *
     * @param  \App\Models\Remessa  $remessa
     * @return void
     */
    public function deleted(Remessa $remessa)
    {
        //
    }

    /**
     * Handle the Remessa "restored" event.
     *
     * @param  \App\Models\Remessa  $remessa
     * @return void
     */
    public function restored(Remessa $remessa)
    {
        //
    }

    /**
     * Handle the Remessa "force deleted" event.
     *
     * @param  \App\Models\Remessa  $remessa
     * @return void
     */
    public function forceDeleted(Remessa $remessa)
    {
        //
    }
}
